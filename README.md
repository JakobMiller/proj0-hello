Author: Jakob Miller

Contact Email: JakobM569@gmail.com

Description: Basic program that uses python code "hello.py" to read a local config file "credentials.ini" to print the message found in the config file. Created primarily to practice and test git processes.
